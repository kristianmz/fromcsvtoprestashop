#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';

use \ImporterBundle\Command\ImporterCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new ImporterCommand());
$application->run();