<?php

namespace ImporterBundle\Command;

use Doctrine\ORM\EntityManager;
use ImporterBundle\Entity\ProductsRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImporterCommand extends Command
{
    /**
     * @var ProductsRepository
     */
    private $productsRepository;

    /**
     * ImporterCommand constructor.
     *
     * @param null               $name
     * @param ProductsRepository $productsRepository
     */
    public function __construct(
        $name = null,
        ProductsRepository $productsRepository
    ) {
        $this->productsRepository = $productsRepository;
        parent::__construct($name);
    }


    protected function configure()
    {
        $this
            ->setName('importer:list_products')
            ->setDescription('Importer');
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        // First get the webservice
        $products = $this->productsRepository->findAll();

        // create a new progress bar (50 units)
        $progress = new ProgressBar($output, count($products));

        // start and displays the progress bar
        $progress->start();

        // Foreach found products
        foreach($products as $product){


            // advance the progress bar 1 unit
            $progress->advance();
        }

        $name = $input->getArgument('name');

        // ensure that the progress bar is at 100%
        $progress->finish();

        $output->writeln($text);
    }
}